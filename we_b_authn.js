function reg() {
  document.getElementById("reg_message").innerHTML = ""
  const xhttp = new XMLHttpRequest();
  xhttp.onload = function() {
    try {
      // Take the registration options, and send them to the hardware
      var reg_opts = {"publicKey": JSON.parse(this.responseText)}
      reg_opts.publicKey.challenge = new TextEncoder().encode(reg_opts.publicKey.challenge)
      reg_opts.publicKey.user.id = new TextEncoder().encode(reg_opts.publicKey.user.id)
      navigator.credentials.create(reg_opts).then(
        function(resp) {
          ao = btoa(String.fromCharCode.apply(null, new Uint8Array(resp.response.attestationObject)))
          cdj = btoa(String.fromCharCode.apply(null, new Uint8Array(resp.response.clientDataJSON)))
          reg_obj = {"id": resp.id, "rawId": resp.id, "type": resp.type, 
                 "response": {"attestationObject": ao, "clientDataJSON": cdj}
                }
          const post = new XMLHttpRequest();
          post.onload = function() {
            document.getElementById("reg_message").innerHTML = this.responseText
          }
          post.open("POST", "register", true)
          post.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
          post.send(JSON.stringify(reg_obj))
        },
        function(resp) {
            console.log("I promise I'll give you an error: " + resp)
            document.getElementById("reg_message").innerHTML = resp
        }
      );
    } catch (err) {
      // Likely an error getting the registation options
      document.getElementById("reg_message").innerHTML = this.responseText
    }
  }
  xhttp.open("GET", "register?user_name=" + document.getElementById("user_name").value)
  xhttp.send()

  return false;  // Don't submit the form
}

function auth() {
  document.getElementById("auth_message").innerHTML = ""
  const xhttp = new XMLHttpRequest();
  xhttp.onload = function() {
    var auth_opts = {"publicKey": JSON.parse(this.responseText)}
    auth_opts.publicKey.challenge = new TextEncoder().encode(auth_opts.publicKey.challenge)
    b64 = auth_opts.publicKey.allowCredentials[0]["id"].replaceAll("_", "/").replaceAll("-", "+")
    var raw = atob(b64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));
    for(i = 0; i < rawLength; i++) {array[i] = raw.charCodeAt(i);}
      auth_opts.publicKey.allowCredentials[0]["id"] = array
    navigator.credentials.get(auth_opts).then(
      function(resp) {
        id = resp.id
        t = resp.type
        uh = resp.response.userHandle
        ad = btoa(String.fromCharCode.apply(null, new Uint8Array(resp.response.authenticatorData)))
        cdj = btoa(String.fromCharCode.apply(null, new Uint8Array(resp.response.clientDataJSON)))
        sig = btoa(String.fromCharCode.apply(null, new Uint8Array(resp.response.signature)))
        auth_data = {"id": id, "rawId": id, "response": {"authenticatorData": ad, "clientDataJSON": cdj, "signature": sig, "userHandle": uh}, "type": t}
        const post = new XMLHttpRequest();
        post.onload = function() {
          document.getElementById("auth_message").innerHTML = this.responseText
        }
        post.open("POST", "auth", true)
        post.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
        post.send(JSON.stringify(auth_data))
      },
      function(resp) {
        console.log("I promise I'll give you an error: " + resp)
        document.getElementById("auth_message").innerHTML = resp
      }
    )
  }

  xhttp.open("GET", "auth?user_name=" + document.getElementById("user_name").value)
  xhttp.send()
  return false;  // Don't submit the form
}
