We all want the passwordless future that has been promised to us, and that
has finally arrived at the web.  But why don't websites implement it?

The existing guides out there are really complicated or require a bunch of
huge dependencies like node.js.  This repo aims to solve that.   It's as
simple as possible, doesn't require any JS libraries and the Python code
should be able to be easily adapted to Java, PHP, or whatever other
language you're using.

So check it out, and bring the passwordless future to your users!

# User experience
- Go to page
- Register
  - Enter username and click Register
  - Enter PIN on hardware security module and tap to accept
- Login
  - Click login
  - Tap accept on hardware key to log in

Note: Users' configuration, browser, and hardware may have a slightly
different user experience.  For example, Yubikey has users enter the PIN
on their computer, rather than on the hardware device.  The description
above is for a Trezor in Opera browser.

# Diagrams
The registration process from the browser's side is to request a challenge
and options from the server, send those to the hardware to be signed, and
then return the signed data to prove that you control the keys that you
are using to register.

![Registration diagram](docs/webauthn-registration.png)

The authentication process is very similar.  The browser requests a challenge
from the server, sends it to the hardware to be signed, and returns the
signature.

![Authentication diagram](docs/webauthn-authentication.png)

# Dependencies
The server side requires the `webauthn` python library.  To get it:

```sh
pip3 install -r requirements.txt || pip3 install webauthn
```

If you don't have pip installed, on Debian-based systems you can get it with:

```sh
apt install -y python3-pip || sudo apt install -y python3-pip
```

# Setup
Browsers require sites using Webauth to use HTTPS.  There are a number of
ways you can accomplish this, but the for a quick demo, the easiest is
probably to just have an existing web server that has TLS support to proxy
requests to this little demo app.

For production, you'll want to integrate the code or logic into your own
system.

## Apache 2.4
In apache2, run `a2enmod proxy proxy_http` and then add the following lines
inside the `<Virtualhost *:443>` section of your configuration file:

```
ProxyPass /webauthn_demo http://localhost:8080
ProxyPassReverse /webauthn_demo http://localhost:8080
```

After that, restart apache and going to the `webauthn_demo` directory
on your website should let you get to this demo webserver

## Nginx
TODO


# Documentation
- [webauthn.guide](https://webauthn.guide/)
- [MDN: Webauthn API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Authentication_API)
- [MDN: navigator.credentials.create](https://developer.mozilla.org/en-US/docs/Web/API/AuthenticatorAttestationResponse/attestationObject)
- [MDN: another page on navigator.credentials.create](https://developer.mozilla.org/en-US/docs/Web/API/CredentialsContainer/create)
- [MDN: navigator.credentials.get](https://developer.mozilla.org/en-US/docs/Web/API/CredentialsContainer/get)
- [MDN: PublicKeyCredential](https://developer.mozilla.org/en-US/docs/Web/API/PublicKeyCredential)
- [Yubikey's Webauthn walk-through](https://developers.yubico.com/WebAuthn/WebAuthn_Walk-Through.html)
- [Yubikey's overview](https://developers.yubico.com/WebAuthn/)
- [DuoLab's Python webauthn library](https://github.com/duo-labs/py_webauthn)

# Alternatives
There are other projects that claim to be simple, but then have huge
dependencies like installing node.js and npm.  However, if you are
in an envrionment where you already have these installed, you may be
interested in
[webauthn-simple-app](https://github.com/webauthn-open-source/webauthn-simple-app).

There's also [webauthn.bin.coffee](https://github.com/jcjones/webauthn.bin.coffee)
which is over 1000 lines of javascript, as compared to this implementation which
is less than 100.  Still, it did help me understand some of the things that were
not clear in the MDN and W3C documentation.

# Greetz
- borismus - for providing JS to [convert a base64 string to binary](https://gist.github.com/borismus/1032746)
             which totally should be a native feature in JS, but it is not
- Duo Labs - for their [Python webauthn library](https://github.com/duo-labs/py_webauthn)
- Trezor - for making [open](https://github.com/trezor/trezor-hardware)
           [source](https://github.com/trezor/trezor-core/blob/master/docs/hardware.md)
           [hardware](https://wiki.trezor.io/Open-source_hardware)
           [that supports FIDO2](https://trezor.io/fido2/)
